import { NextPage } from 'next'
import Sidebar from '@components/Home/Sidebar'

const Home: NextPage = () => {
  return (
    <div className="bg-black h-screen overflow-hidden">
      <main>
        <Sidebar />
        {/*Center*/}
      </main>

      <div>{/*Player*/}</div>
    </div>
  )
}

export default Home
